#!/usr/bin/python
# -*- coding: utf-8 -*-

#BIBLIOTECA MCP3008
import logging
import sys
import time
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008

#Bibliotecas MQTT
import paho.mqtt.client as paho
from struct import pack
from random import randint
from time import sleep

from Adafruit_BNO055 import BNO055

bno = BNO055.BNO055(serial_port='/dev/ttyAMA0', rst=17)
CLK  = 18
MISO = 23
MOSI = 24
CS   = 25
temp="config/embarcado/numero"

#RECEBE CONFIG MOBILE (Bloqueante até receber os dados) -----------------------------------------------
amostras=''
intervalo=''
def on_connect(client, userdata, flags, rc):
  print("Connected with result code "+str(rc))
  client.subscribe("Configuracao")
def on_message(client, userdata, msg):
    global amostras
    global intervalo
    (amostras,intervalo)=msg.payload.split(';')
    print("OLA:"+amostras)
    print("OLAKATIO::::"+intervalo)
    client.disconnect()
client = paho.Client()
client.connect("test.mosquitto.org",1883,60)
client.on_connect = on_connect
client.on_message = on_message
client.loop_forever()
#--------------------------------------------------------------------

#Configurações para publicação --------------------------------------
# cria um identificador baseado no id do sensor
client = paho.Client(client_id = 'Sensor',protocol = paho.MQTTv311)
#client.username_pw_set("embarcado", "123456")

# conecta no broker
client.connect("test.mosquitto.org", 1883)
#-------------------------------------------------------------------

#Inicializa o conversor AD para leituras analógicas-----------------
mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)
#-------------------------------------------------------------------

#Configurações BNO055 ----------------------------------------------
# Enable verbose debug logging if -v is passed as a parameter.
#if len(sys.argv) == 2 and sys.argv[1].lower() == '-v':
#    logging.basicConfig(level=logging.DEBUG)

# Inicializa o sensor de Movimento.
#if not bno.begin():
#    raise RuntimeError('Falha ao iniciar o sensor! Está corretamente ligado ?')
#-------------------------------------------------------------------

#Leitura da configuração
#nb=input('Ecolha o número de amostras a serem coletadas:')
#am=input('Digite o intervalo entre as amostras coletadas:')
try:
    temp=int(amostras)
    nm=float(intervalo)
except ValueError:
    print("Invalid Numer")

i=0;
print('| X    | Y    | Z    | Muscle |')
print('-' * 57)
tempo=temp*60;
while i<tempo:
    # Read the Euler angles for heading, roll, pitch (all in degrees).
 #   heading, roll, pitch = bno.read_euler()
    # Read the calibration status, 0=uncalibrated and 3=fully calibrated.
  #  sys, gyro, accel, mag = bno.get_calibration_status()
    values = mcp.read_adc(0)
    # Print everything out.
   # print('| {0:>0.2F} | {1:>0.2F} | {2:>0.2F} | {3:>0.2F} '.format(roll,pitch,heading,values))
    #client.publish(temp,values,qos=0)
    print 'Teste' + ":" + str(values)  
    time.sleep(nm)
    i=i+temp
